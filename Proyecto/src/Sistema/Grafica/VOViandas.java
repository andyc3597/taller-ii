package Sistema.Grafica;

public class VOViandas {
	private String codigo;
	private String descripcion;
	private int precio;
	
	public VOViandas (String cod, String des, int pre) { //Vegetariana? Incluir atributos?
		codigo = cod;
		descripcion = des;
		precio = pre;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public int getPrecio() {
		return precio;
	}
}
