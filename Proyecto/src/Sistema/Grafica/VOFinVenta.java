package Sistema.Grafica;

import java.io.Serializable;

public class VOFinVenta implements Serializable{
	
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	private int numVen;
	private int opcion;
	
	// Constructor
	public VOFinVenta(int num, int opc){
		numVen = num;
		opcion = opc;
	}
	
	// Selectoras
	public int getNumVen() {
		return numVen;
	}
	
	public int getOpcion() {
		return opcion;
	}
}
