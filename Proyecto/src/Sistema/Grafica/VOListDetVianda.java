package Sistema.Grafica;

public class VOListDetVianda extends VOVianda{
	public boolean ovolactea;
	public String descripcionAd;
	
	public VOListDetVianda(String cod, String des, int pre, boolean ovo, String desAd) {
		super (cod, des, pre);
		ovolactea = ovo;
		descripcionAd = desAd;
	}
	
	public boolean getOvolactea() {
		return ovolactea;
	}
	
	public String getDescripcionAd() {
		return descripcionAd;
	}
}