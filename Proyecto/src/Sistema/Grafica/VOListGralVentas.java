package Sistema.Grafica;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

public class VOListGralVentas implements Serializable{
	
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	private int numero;
	private Date fecha;
	private Time hora;
	private String dire_Entrega;
	private int monto_Total;
	private boolean finalizada;
	
	// Constructor
	public VOListGralVentas (int num, Date fech, Time hor, String dire, int monto, boolean fin) {
		numero = num;
		fecha = fech;
		hora = hor;
		dire_Entrega = dire;
		monto_Total = monto;
		finalizada = fin;
	}
	
	// Selectoras
	public int getNumero() {
		return numero;
	}
	
	public Date getFecha() {
		return fecha;
	}
	
	public Time getHora() {
		return hora;
	}
	
	public String getDireEntrega() {
		return dire_Entrega;
	}
	
	public int getMontoTotal() {
		return monto_Total;
	}
	
	public boolean getFinalizada() {
		return finalizada;
	}
}
