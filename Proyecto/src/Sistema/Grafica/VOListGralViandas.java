package Sistema.Grafica;

public class VOListGralViandas extends VOVianda{
	
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	private boolean vegetariana;
	
	// Constructor
	public VOListGralViandas(String cod, String des, int pre, boolean v) {
		super (cod, des, pre);
		vegetariana = v;
	}
	
	// Selectoras
	public boolean getTipo() {
		return vegetariana;
	}

}
