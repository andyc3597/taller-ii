package Sistema.Grafica;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JToolBar;

public class MenuPpal {

	private JFrame frmMenuPrincipal;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPpal window = new MenuPpal();
					window.frmMenuPrincipal.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MenuPpal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMenuPrincipal = new JFrame();
		frmMenuPrincipal.setTitle("Menu principal");
		frmMenuPrincipal.setBounds(100, 100, 450, 300);
		frmMenuPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMenuPrincipal.getContentPane().setLayout(null);
		
		JButton btnAltaVianda = new JButton("Alta vianda");
		btnAltaVianda.setBounds(10, 11, 89, 23);
		frmMenuPrincipal.getContentPane().add(btnAltaVianda);
		
		JButton btnVenta = new JButton("Venta");
		btnVenta.setBounds(10, 45, 89, 23);
		frmMenuPrincipal.getContentPane().add(btnVenta);
		
		JButton btnListados = new JButton("Listados");
		btnListados.setBounds(10, 164, 89, 23);
		frmMenuPrincipal.getContentPane().add(btnListados);
		
		JLabel lblNewLabel = new JLabel("Nueva venta");
		lblNewLabel.setBounds(49, 68, 70, 14);
		frmMenuPrincipal.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Agregar vianda");
		lblNewLabel_1.setBounds(49, 93, 89, 14);
		frmMenuPrincipal.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Eliminar vianda");
		lblNewLabel_2.setBounds(49, 114, 113, 14);
		frmMenuPrincipal.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Finalizar venta");
		lblNewLabel_3.setBounds(49, 139, 89, 14);
		frmMenuPrincipal.getContentPane().add(lblNewLabel_3);
	}
}
