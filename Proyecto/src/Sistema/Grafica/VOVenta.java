package Sistema.Grafica;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

public class VOVenta implements Serializable{
	
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	private int numero;
	private Date fecha;
	private Time hora;
	private String Dire_Entrega;
	private int cantTotal;
	
	// Constructor
	public VOVenta (int num, Date fech, Time hor, String dire) {
		numero = num;
		fecha = fech;
		hora = hor;
		Dire_Entrega = dire;
	}
	
	// Selectoras
	public int getNumero() {
		return numero;
	}
	
	public Date getFecha() {
		return fecha;
	}
	
	public Time getHora() {
		return hora;
	}
	
	public String getDireEntrega() {
		return Dire_Entrega;
	}
	
	public int getCantTotal() {
		return cantTotal;
	}
}
