package Sistema.Grafica;

import java.io.Serializable;

public class VOAgrElimVianda implements Serializable{
	
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	private String codVia;
	private int numVen;
	private int cantidad;
	
	// Constructor
	public VOAgrElimVianda(String cod, int num, int cant){
		codVia = cod;
		numVen = num;
		cantidad = cant;
	}
	
	// Selectoras
	public String getCodVia() {
		return codVia;
	}
	
	public int getNumVen() {
		return numVen;
	}
	
	public int getCantidad() {
		return cantidad;
	}
}
