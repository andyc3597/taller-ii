package Sistema.Grafica;

public class VODetViandaVen extends VOVianda {

	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	private int cant;
	private String obs;
	
	// Constructor
	public VODetViandaVen(String cod, String des, int pre, int cantidad, String observaciones) {
		super (cod, des, pre);
		cant = cantidad;
		obs = observaciones;
	}
	
	// Selectoras
	public int getCant() {
		return cant;
	}
	
	public String getObs() {
		return obs;
	}
}
