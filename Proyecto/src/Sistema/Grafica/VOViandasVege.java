package Sistema.Grafica;

import java.io.Serializable;

public class VOViandasVege extends VOVianda implements Serializable{
	
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	private boolean ovolactea;
	private String descripAd;
	
	// Constructor
	public VOViandasVege (String cod, String des, int pre, boolean ovo, String descAd) {
		super(cod, des, pre);
		ovolactea = ovo;
		descripAd = descAd;
	}
	
	// Selectoras
	public boolean getOvolactea() {
		return ovolactea;
	}
	
	public String getDescripAD() {
		return descripAd;
	}
}
