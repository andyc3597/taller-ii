package Sistema.Grafica;

public class VOListViandasEnVenta extends VOVianda{

	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	int CantUnidades;
	private String descripAd;
	
	// Constructor
	public VOListViandasEnVenta (String cod, String des, int pre, int cantUni, String descAd) {
		super(cod, des, pre);
		CantUnidades = cantUni;
		descripAd = descAd;
	}
	
	// Selectoras
	public int getCantUnidades() {
		return CantUnidades;
	}
	
	public String getDescripAD() {
		return descripAd;
	}
}
