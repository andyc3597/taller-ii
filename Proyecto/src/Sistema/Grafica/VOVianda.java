package Sistema.Grafica;

import java.io.Serializable;

public class VOVianda implements Serializable{
	
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	private String codigo;
	private String descripcion;
	private int precio;
	
	// Constructor
	public VOVianda (String cod, String des, int pre) {
		codigo = cod;
		descripcion = des;
		precio = pre;
	}
	
	// Selectoras
	public String getCodigo() {
		return codigo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public int getPrecio() {
		return precio;
	}
}
