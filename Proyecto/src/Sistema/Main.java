package Sistema;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Properties;

import Sistema.Excepciones.PersistenciaException;
import Sistema.Excepciones.ViandaException;
import Sistema.Logica.*;

public class Main {	
	public static void main(String[] args) throws ViandaException, RemoteException, PersistenciaException {
		/*
		El main se encargara unicamente de:
			1. Intentar levantar de archivo la información de las colecciones.
			
			
			
			2. Si pudo hacerlo, crear la fachada con dichas colecciones.
			3. Si no pudo hacerlo (porque es la primera vez que se ejecuta), crear la fachada
			con las colecciones vacías.
			
			puntos 2 y 3 van adentro del constructor de fachada
			
			4. Una vez creada la fachada, publicarla como objeto remoto, para que quede
			disponible para los programas cliente que luego se conectarán remotamente.
			
			con el naming.rebind
		*/
		
		try{
			Properties p = new Properties();
			String nomArch = "config/config.properties";
			// Abro el archivo properties y leo los datos de configuración
			p.load (new FileInputStream (nomArch));
			String ip = p.getProperty("ipServidor");
			String puerto = p.getProperty("puertoServidor");
			
			// instancio mi Objeto Remoto y lo publico en el rmiregistry
			// pongo a correr el rmiregistry
			LocateRegistry.createRegistry(1099);
			// instancio mi Objeto Remoto y lo publico en la ruta especificada
			Fachada fachada = Fachada.getFachada();
			String ruta = "//" + ip + ":" + puerto + "/fachada";
			
			System.out.println ("Antes de publicarlo");
			Naming.rebind(ruta, fachada);
			System.out.println ("Luego de publicarlo");
			
		}catch (RemoteException e){ 
			e.printStackTrace(); 
		}catch (MalformedURLException e){ 
			e.printStackTrace(); 
		}catch (IOException e){
			e.printStackTrace();
		}
	}
}
