package Sistema.Logica;

public class Monitor {
	private boolean escribiendo;
	private int cantLectores;
	
	
	public Monitor (boolean escrib, int cantL) {
		escribiendo = escrib;
		cantLectores = cantL;
	}
	
	public Monitor(){
		escribiendo = false;
		cantLectores = 0;
	}

	public boolean getEscribiendo() {
		return escribiendo;
	}
	
	public int getCantLectores() {
		return cantLectores;
	}
	
	public synchronized void comienzoLectura() {
		try{
			if(this.escribiendo){
				wait();
			}else{
				this.cantLectores++;
			}
		}catch(InterruptedException e){
			e.printStackTrace();	
		}
	}
	
	public synchronized void terminoLectura() {
		this.cantLectores--;
		if(this.cantLectores == 0){
			notify();
		}
	}
	
	public synchronized void comienzoEscritura() {
		if(this.escribiendo || this.cantLectores !=0){
			try{
				wait();
			}catch(InterruptedException e) {
				e.printStackTrace();	
			}
		}else{
			this.escribiendo = true;
		}
	}
	
	public synchronized void terminoEscritura() {
		this.escribiendo = false;
		notify();
	}

}
