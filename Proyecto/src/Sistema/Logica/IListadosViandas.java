package Sistema.Logica;

import java.rmi.Remote;
import java.rmi.RemoteException;

//import excepciones.NoExisteException;

public interface IListadosViandas extends Remote {
		
	public void ListadoGeneral () throws RemoteException; //NoExisteException;
	
	public void ListadoDetallado (String cod) throws RemoteException; //NoExisteException;
	
	public void ListadoPorDescrip (String descrip) throws RemoteException; //NoExisteException;
}