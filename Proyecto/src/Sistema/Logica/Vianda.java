package Sistema.Logica;
import java.io.Serializable;

public class Vianda implements Serializable{
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	private String codigo;
	private String descripcion;
	private int precio;
	
	// Constructor
	public Vianda (String cod, String desc, int pre) {
		codigo = cod;
		descripcion = desc;
		precio = pre;
	}
	
	// Selectoras
	public String getCodigo() {
		return codigo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public int getPrecio() {
		return precio;
	}
}
