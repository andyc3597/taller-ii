package Sistema.Logica;

public class Vegetariana extends Vianda {
	
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	private boolean ovolactea;
	private String descripcionAd;
	
	// Constructor
	public Vegetariana (String cod, String desc, int pre, boolean ovo, String descAd) {
		super (cod, desc, pre);
		ovolactea = ovo;
		descripcionAd = descAd;
	}
	
	// Selectoras
	public boolean getOvolactea() {
		return ovolactea;
	}
	
	public String getDescripcionOvo() {
		return descripcionAd;
	}
}
