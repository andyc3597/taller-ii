package Sistema.Logica;

import java.sql.Time;
import java.util.Date;
import java.io.Serializable;

public class Venta implements Serializable{
	// Codigo automatico
	private static final long serialVersionUID = 1L; // Sugerencia para el warning, el final hace que no se pueda sobreescribir el metodo en una clase derivada
	
	// Atributos
	private int numero;
	private Date fecha;
	private Time hora;
	private String dire_Entrega;
	private boolean finalizada;
	private int cant_Total;
	private Poseen pos;
	
	// Constructor
	public Venta (int num, Date fech, Time hor, String dir) {
		numero = num;
		fecha = fech;
		hora = hor;
		dire_Entrega = dir;
		finalizada = false; // Las ventas por defecto se inicializan sin finalizar
		cant_Total = 0; // Inicializamos la cantidad total en 0
		pos = new Poseen(); // Y tambien se inicializa la secuencia vacia
	}
	
	// Selectoras
	public int getNumero() {
		return numero;
	}
	
	public Date getFecha() {
		return fecha;
	}
	
	public Time getHora() {
		return hora;
	}
	
	public String getDirEntrega() {
		return dire_Entrega;
	}
	
	public boolean getFinalizada() {
		return finalizada;
	}
	
	public int getCantTotal() {
		return cant_Total;
	}	
	
	public Poseen getPoseen() {
		return pos;
	}

	// Seteadoras
	public void setCantTotal(int cant) {
		cant_Total = cant;
	}
	
	public void setFinalizada() {
		finalizada = true;
	}
}
