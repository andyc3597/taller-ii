package Sistema.Logica;
import java.io.Serializable;
import java.util.*;

import Sistema.Grafica.*;
import Sistema.Logica.Vianda;

public class Viandas implements Serializable{
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	//Arbol
	TreeMap <String, Vianda> Viandas = new TreeMap <String, Vianda>();
	
	private static Viandas viandas;
	
	
	// Metodos primitivos
	public void insert (String cod, Vianda vian){ 
		Viandas.put(cod, vian);
	}
	
	public boolean member(String cod){ 
		return Viandas.containsKey(cod); 
	}
	
	public Vianda find (String cod){ 
		return Viandas.get(cod); 
	}
	
	// Metodos especificos
	
	public boolean EsVacio(){ 
		return Viandas.isEmpty();
	}
	
	// Por el requerimiento 10
	public ArrayList<VOListGralViandas> ListarViandas() {
		ArrayList<VOListGralViandas> arre = new ArrayList<VOListGralViandas>();
		
		for (Vianda vian: Viandas.values()) {
			
			// Obtener los datos de la vianda
			String cod = vian.getCodigo();
			String desc = vian.getDescripcion();
			int prec = vian.getPrecio();
			boolean vege = false; // Por defecto asumo que es una vianda normal
			
			if(vian instanceof Vegetariana){ // Si es una vianda vegetariana, cambio el boolean a true
				vege = true;
			}
				
			// Crear el value object
			VOListGralViandas vv = new VOListGralViandas(cod, desc, prec, vege);
			
			// Lo inserto en el arreglo
			arre.add(vv);
		}
		return arre;
	}
	
	// Por el requerimiento 11
	public VOVianda ListarDetalleVianda(String codVian) {
		
		// Creo el VO a devolver
		VOVianda vov = null;
		
		// Obtengo la vianda
		Vianda vian = viandas.find(codVian);
		
		// Obtener los datos de la vianda para el VO
		String cod = vian.getCodigo();
		String desc = vian.getDescripcion();
		int prec = vian.getPrecio();
			
		if(vian instanceof Vianda){ // Si es una vianda normal
			
			// Crear el objeto vianda normal para devolver
			vov = new VOVianda(cod, desc, prec);
			
		}else{ // Si es una vianda vegetariana
			
			// Cargo los datos extras de vegetariana
			boolean ovo = ((Vegetariana) vian).getOvolactea(); 
			String descripOvo = ((Vegetariana) vian).getDescripcionOvo(); 	
						
			// Crear el objeto vegetariana para devolver
			vov = new VOViandasVege(cod, desc, prec, ovo, descripOvo);
			
		}
		return vov;
	}
	
	public ArrayList<VOListGralViandas> ListarViandasDescripcion(String descUser) {
		// Itero sobre todas las viandas buscando coincidencias
		ArrayList<VOListGralViandas> arre = new ArrayList<VOListGralViandas>();
		
		for (Vianda vian: Viandas.values()) {
			// Obtengo la descripcion de la vianda
			
			if(vian.getDescripcion().contains(descUser)) {// Si la descripcion coincide con la de alguna vianda
				String cod = vian.getCodigo();
				String desc  = vian.getDescripcion();
				int prec = vian.getPrecio();
				boolean veg = false;
				
				if (vian instanceof Vegetariana) {
					veg = true;
				}
				
				VOListGralViandas vv = new VOListGralViandas(cod, desc, prec, veg);
				arre.add(vv);
			}
		}

		return arre;
	}
	
	/*
	public Vianda[] GuardarViandas() { // Codigo para guardar la coleccion en un archivo
		Vianda arre[] = new Vianda[Viandas.size()];
		//Va a ser un arreglo polimorfico de viandas
		
		for (Vianda vian: Viandas.values()) {
			int i = 0;
			
			// Obtener los datos de la venta
			String cod = vian.getCodigo();
			String desc = vian.getDescripcion();
			int prec = vian.getPrecio();
			
			if(vian instanceof Vegetariana){ // Si es una vianda vegetariana
				boolean vege = ((Vegetariana) vian).getOvolactea(); 
				String descrip = ((Vegetariana) vian).getDescripcionOvo(); 	
				// Crear el objeto vegetariana para insertar en el arreglo
				Vianda av = new Vegetariana(cod, desc, prec, vege, descrip);
				
				// Lo inserto en la celda correspondiente del arreglo
				arre[i] = av;
			 }else{ // Si es una vianda normal 
				// Crear el objeto vianda normal para insertar en el arreglo
				Vianda av = new Vianda(cod, desc, prec);
				
				// Lo inserto en la celda correspondiente del arreglo
				arre[i] = av;
			 }
			
			i++;
		}
		return arre;
	}
	
	public void RestaurarViandas() {
		// Codigo para restaurar la coleccion desde un archivo
	}
	*/
}
