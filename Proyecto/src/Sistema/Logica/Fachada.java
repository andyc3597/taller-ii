/* PENDIENTES:
 	- Testing en general:
 	
	- Probar RMI, correr as Java aplication una clase de pruebas y usar el lookup para obtener fachada y metodos,
	  esto mientras el main se corre en paralelo, al ser diferentes hilos tecnicamente se aplica RMI
	- Probar el singleton de fachada, con un println en el getFachada para ver si crea una nueva o la obtiene
	
	- Probar todos los requerimientos de fachada
	- Probar que aparezcan las excepciones en pantalla correspondientes a los errores
	- Agregar excepcion en fachada recuperar y respaldar, persistenciaException que agrego profe, mejorarlo
	- Agregar excepcion en monitor de error de concurrencia
	
	- Obtener nombre de archivo para respaldar a traves del archivo de configuracion
*/

package Sistema.Logica;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

import Sistema.Excepciones.*;
import Sistema.Grafica.*;
import Sistema.Persistencia.Persistencia;
import Sistema.Persistencia.Contenedor;;

public class Fachada extends UnicastRemoteObject implements IFachada{
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	private Viandas viandas;
	private Ventas ventas;
	private Monitor monitor;
	
	private static Fachada fachada;
	
	//Constructor
	private Fachada() throws PersistenciaException, RemoteException{
		
		// Inicializo monitor
		monitor = new Monitor();
		
		// Intento obtener los datos de los archivos, en caso de que no pueda, creo nuevos
		RestaurarCambios();
		// agregar excpecion archivo noexiste
	}
	
	// Aplico el singleton, para tener siempre solo una unica instancia de la fachada
	public static Fachada getFachada() throws PersistenciaException, RemoteException{
		if (fachada == null)
			fachada = new Fachada();
		
		return fachada;
	}
	
	// Requerimientos
	
	// Requerimiento 1
	public void NuevaVianda (VOVianda vovia) throws ViandaException { 
		monitor.comienzoEscritura();
		String cod = vovia.getCodigo();
		
		if(!viandas.member(cod)) {
			//Instancio un objeto vianda con los datos del valueobject
			String descrip = vovia.getDescripcion();
			int precio = vovia.getPrecio();
			Vianda via = new Vianda(cod, descrip, precio);
			
			// Insertar vianda en la coleccion
			viandas.insert(cod, via);
			monitor.terminoEscritura();
		}else {
			monitor.terminoEscritura();
			throw new ViandaException(1, "Vianda ya registrada.");
		}

	}
	
	// Requerimiento 2
	public void NuevaVenta (VOVenta voven) throws VentaException {
		monitor.comienzoEscritura();
		
		Venta vent = ventas.Back(); // Me quedo con la ultima venta
		
		// Uso el entero comp para comparar las fechas el metodo dado por la clase Fecha
		int comp = vent.getFecha().compareTo(voven.getFecha()); // Devuelve 0 si son iguales, 1 si es mayor, y -1 si es menor
		if (comp == 1 || comp == 0) {
			if(voven.getHora().after(vent.getHora())) { // Si la hora tambien es posterior
				
				//Instancio un objeto venta con los datos del valueobject y los por defecto (Numero y Finalizado)
				int num = vent.getNumero() + 1; // Generamos el codigo autonumber incremental de la nueva venta
				Date fech = voven.getFecha();
				Time hor = voven.getHora();
				String direc = voven.getDireEntrega();

				Venta ven = new Venta(num, fech, hor, direc);
				
				//Insertarla en la secuencia de ventas
				ventas.InsBack(ven);
				monitor.terminoEscritura();
		
			}else{
				monitor.terminoEscritura();
				throw new VentaException(5, "La hora ingresada no es posterior a la de la �ltima venta.");
			}
		}else {
			monitor.terminoEscritura();
			throw new VentaException(4, "La fecha ingresada no es posterior a la de la �ltima venta.");
		}
	}
	
	// Requerimiento 3
	public void AgregarVianda (VOAgrElimVianda vo) throws ViandaException, VentaException{
		monitor.comienzoEscritura();
		if(viandas.member(vo.getCodVia())) {
			if(ventas.ExisteVenta(vo.getNumVen())) {
				Venta vent = ventas.ObtenerVenta(vo.getNumVen()); // Obtengo la venta
				if(vent.getFinalizada() == false){ // Chequeo que este en proceso
					int total = vo.getCantidad() + vent.getCantTotal();
					if(total <= 30){ // Chequeo que la suma de la cantidad total y la cantidad ingresada no superen las 30
						vent.setCantTotal(total); // Actualizo la cantidad total de venta
						
						// Me quedo con la vianda
						Vianda via = viandas.find(vo.getCodVia()); 
						// Chequeo si existe esa vianda en la coleccion Poseen
						boolean existe = vent.getPoseen().PoseenExisteVianda(via);
						
						if(existe) { // Si existe, actualizamos la cantidad
							
							// Sumo la cantidad pasada en el VO con la cantidad ya existente de esa vianda en Posee
							int cantPosVia = vo.getCantidad() + vent.getPoseen().getPosee(via).getCantidad();
							
							// Le actualizo la cantidad al Posee para esa vianda
							vent.getPoseen().getPosee(via).setCantidad(cantPosVia);
							
							monitor.terminoEscritura();
						}else { // Si no existe insertamos esa vianda en Poseen con la cantidad ingresada
							
							// Creo el objeto posee, con la vianda y cantidad del VO
							Posee pos = new Posee(via, vo.getCantidad());
							
							// Lo inserto al final de la secuencia Poseen de la venta
							vent.getPoseen().InsBack(pos);
							
							monitor.terminoEscritura();			
						}
					}else{
						monitor.terminoEscritura();
						throw new VentaException(3, "No puede sobrepasar las 30 viandas por venta.");
					}
				}else {
					monitor.terminoEscritura();
					throw new VentaException(2, "La venta ya se encuentra finalizada.");
				}
			}else {
				monitor.terminoEscritura();
				throw new VentaException(1, "La venta a la que hace referencia no existe.");
			}
		}else {
			monitor.terminoEscritura();
			throw new ViandaException(3, "La vianda ingresada no est� registrada.");
		}
	}
	
	// Requerimiento 4
	public void EliminarVianda (VOAgrElimVianda vo) throws ViandaException, VentaException {
		monitor.comienzoEscritura();
		if(viandas.member(vo.getCodVia())) {
			if(ventas.ExisteVenta(vo.getNumVen())) {
				Venta vent = ventas.ObtenerVenta(vo.getNumVen()); // Obtengo la venta
				if(vent.getFinalizada() == false){ // Chequeo que este en proceso
					
					Vianda vian = viandas.find(vo.getCodVia()); // Obtengo la vianda
					Posee pos = vent.getPoseen().getPosee(vian); // Obtengo el objeto posee referente a esa vianda
					
					if(vent.getPoseen().PoseenExisteVianda(vian)){ // Chequear si existe esa vianda en la coleccion Poseen, si existe
						if(vo.getCantidad() >= pos.getCantidad()){ // Chequeo que cantidad a eliminar no supera a la cantidad de Posee
							if(vo.getCantidad() == pos.getCantidad()) {
								int total = vent.getCantTotal() - vo.getCantidad();
								vent.setCantTotal(total); // Actualizo la cantidad total de venta
								
								vent.getPoseen().PoseenEliminarVianda(pos); // Eliminar el objeto posee por completo de la venta
								monitor.terminoEscritura();
							}else {
								int total = vent.getCantTotal() - vo.getCantidad();
								vent.setCantTotal(total); // Restamos y actualizamos la cantidad total de venta
								
								pos.setCantidad(pos.getCantidad() - vo.getCantidad()); // Restarle la cantidad ingresada a la cantidad de Poseen
								monitor.terminoEscritura();
							}
						}else { // Si no existe insertamos esa vianda en Poseen con la cantidad ingresada
							monitor.terminoEscritura();
							throw new ViandaException(5, "La cantidad ingresada supera a la cantidad de viandas de ese tipo.\n");
						}
					}else {
						monitor.terminoEscritura();
						throw new ViandaException(4, "La vianda ingresada no existe en la coleccion.\n");
					}
				}else {
					monitor.terminoEscritura();
					throw new VentaException(2, "La venta ya se encuentra finalizada.\n");
				}
			}else {
				monitor.terminoEscritura();
				throw new VentaException(1, "La venta a la que hace referencia no existe.\n");
			}
		}else {
			monitor.terminoEscritura();
			throw new ViandaException(3, "La vianda ingresada no est� registrada.\n");
		}
	}
	
	// Requerimiento 5
	public void FinalizarVenta (VOFinVenta vo) throws VentaException {
		monitor.comienzoEscritura();
		if(ventas.ExisteVenta(vo.getNumVen())) {
			Venta vent = ventas.ObtenerVenta(vo.getNumVen()); // Si la venta existe, la obtengo
			
			if(vo.getOpcion() == 0){ // opc = 0 es porque se desea finalizar la venta 
				if(vent.getCantTotal() == 0) { // Si tiene cantidad total 0 eliminarla de la secuencia de ventas
					ventas.EliminarVenta(vent);
					monitor.terminoEscritura();
				}else { // Marcarla como finalizada
					vent.setFinalizada();
					monitor.terminoEscritura();
				}
			}else { // opc = 1 es porque se desea cancelar la venta 
				ventas.EliminarVenta(vent);
				monitor.terminoEscritura();
			}
		}else {
			monitor.terminoEscritura();
			throw new VentaException(1, "La venta a la que hace referencia no existe.\n");
		}
	}
	
	// Requerimiento 6
	public ArrayList<VOListGralVentas> ListadoGralVentas() throws VentaException { // Listado general de ventas
		monitor.comienzoLectura();
		
		if(!ventas.Empty()) { // Si ventas tiene datos
			// Llamo a listarVentas que devuelve el arreglo de VOListGralVentas
			ArrayList<VOListGralVentas> arre = ventas.ListarVentas();
					
			monitor.terminoLectura();
			
			//Retorno el arreglo de value objects
			return arre;
		}else {
			monitor.terminoLectura();
			throw new VentaException(6, "No se realizaron ventas.\n");
		}
	}
	
	// Requerimiento 7
	// PRECONDICION: La secuencia de ventas no esta vacia
	public ArrayList<VOListViandasEnVenta> ListadoDetViandasVenta(int numVen) throws VentaException { // Listado detallado de las viandas de una venta
		monitor.comienzoLectura();
		
		if(ventas.ExisteVenta(numVen)) {
			Venta vent = ventas.ObtenerVenta(numVen); // Obtengo la venta
			
			ArrayList<VOListViandasEnVenta> arre = ventas.ListarViandasVenta(vent);
			
			monitor.terminoEscritura();
			
			//Retorno el arreglo de value objects
			return arre;
		}else {
			monitor.terminoEscritura();
			throw new VentaException(1, "La venta a la que hace referencia no existe.\n");
		}
	}
	
	// Requerimiento 8
	public void GuardarCambios() throws PersistenciaException { // Guardar a archivo todos los cambios
		monitor.comienzoEscritura();
		if((!viandas.EsVacio()) || (!ventas.Empty())) { // Si alguna de las 2 colecciones tiene datos
			
			// Instancio nuevo contenedor con ventas y viandas como parametro
			Contenedor cont = new Contenedor(viandas, ventas);
			
			// Uso el contenedor y el metodo respaldar 
			// Guardo el objeto contenedor en el archivo con el nombre especificado por parametro
			Persistencia persis = new Persistencia("Respaldo.txt");
			persis.respaldar(cont);
			
		}else { // Si no hay datos en ninguna coleccion
			throw new PersistenciaException(1, "No hay datos para respaldar");
		}
	}

	// Requerimiento 9
	public void RestaurarCambios() throws PersistenciaException { // Respaldar desde un archivo todos los datos
		// Intento obtener los datos de los archivos, en caso de que no pueda, creo nuevos
		try{
			monitor.comienzoLectura();
			
			// Aqui esta el requerimiento 9 de recuperar los datos
			Persistencia recuperar = new Persistencia("Respaldo.txt");
			Contenedor cont = recuperar.recuperar();
			
			// Me guardo lo del contenedor en viandas y ventas
			viandas = cont.getViandas();
			ventas = cont.getVentas();
			monitor.terminoLectura();

		}catch (ClassNotFoundException e){
			//Creo un nuevo diccionario de viandas y una nueva secuencia de ventas
			this.ventas = new Ventas();
			this.viandas = new Viandas();
			monitor.terminoLectura();
			throw new PersistenciaException(2, "Error al recuperar");
		}catch (IOException e){
			//Creo un nuevo diccionario de viandas y una nueva secuencia de ventas
			this.ventas = new Ventas();
			this.viandas = new Viandas();
			monitor.terminoLectura();
			throw new PersistenciaException(2, "Error al recuperar");
		}
	}
	
	// Requerimiento 10
	public ArrayList<VOListGralViandas> ListadoGralViandas() throws ViandaException { // Listado general de viandas
		monitor.comienzoLectura();
		
		if(!viandas.EsVacio()) {
			// Llamo a listarViandas que devuelve el arreglo de VOListGralViandas
			ArrayList<VOListGralViandas> arre = viandas.ListarViandas();
			
			monitor.terminoLectura();
			
			//Retorno el arreglo
			return arre;
		}else{
			monitor.terminoLectura();
			throw new ViandaException(6, "No se tiene ninguna vianda registrada en el sistema.\n");
		}		
	}
	
	// Requerimiento 11
	public VOVianda ListadoDetVianda(String codVian) throws ViandaException{ // Listado detallado de una vianda
		monitor.comienzoLectura();
		
		if(viandas.member(codVian)) { // Si la vianda existe
			// Llamo a ListarDetalleVianda que devuelve la vianda con los datos
			VOVianda vv = viandas.ListarDetalleVianda(codVian);
			monitor.terminoLectura();
			return vv;
		}else{
			monitor.terminoLectura();
			throw new ViandaException(3, "La vianda ingresada no est� registrada.");
		}
	}
	
	// Requerimiento 12
	public ArrayList<VOListGralViandas> ListadoViandasDescrip(String descrip) throws ViandaException {	// Listado de todas las viandas con una descripcion que matchee
		monitor.comienzoLectura();
		
		if(!viandas.EsVacio()) {
			// Llamo a metodo que chequee si existen coincidencias
			ArrayList<VOListGralViandas> arre = viandas.ListarViandasDescripcion(descrip);
			if(arre.size() > 0) {
				monitor.terminoLectura();
				return arre;
			}else {
				monitor.terminoLectura();
				throw new ViandaException(7, "No hay ninguna vianda que coincida con la descripci�n ingresada..\n");
			}
		}else{
			monitor.terminoLectura();
			throw new ViandaException(6, "No se tiene ninguna vianda registrada en el sistema.\n");
		}
	}
}
