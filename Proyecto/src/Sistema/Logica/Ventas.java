package Sistema.Logica;
import java.io.Serializable;
import java.sql.Time;
import java.util.*;

import Sistema.Excepciones.VentaException;
import Sistema.Grafica.VOListGralVentas;
import Sistema.Grafica.VOListViandasEnVenta;
import Sistema.Logica.Venta;

public class Ventas implements Serializable{
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	LinkedList<Venta> Ventas = new LinkedList<Venta>();
	
	private static Ventas ventas;
	
	
	// Metodos 
	public void InsBack(Venta ven){ 
		Ventas.add(ven); 
	}
		
	public boolean Empty (){ 
		return Ventas.isEmpty();
	}
	
	public Venta Back (){ 
		return Ventas.getLast();
	}
	
	public boolean ExisteVenta (int numVen){
		boolean resu = false;
		Iterator<Venta> iter = Ventas.iterator();
		
		while (!resu && iter.hasNext()) {
			int num = iter.next().getNumero(); 
			if(num == numVen) {
				resu = true;
			}
		}
		
		return resu;
	}
	
	public Venta ObtenerVenta (int num){ 
		return Ventas.get(num);
	}
	
	public void EliminarVenta (Venta ven){
		Ventas.remove(ven);
	}
	
	// Metodos especificos 
	public ArrayList<VOListGralVentas> ListarVentas(){
		ArrayList<VOListGralVentas> arre = new ArrayList<VOListGralVentas>();
		Iterator<Venta> iter = Ventas.iterator();
		
		while (iter.hasNext()) {
			// Obtener los datos de la venta que se mostraran en el VO
			int num = iter.next().getNumero(); 
			Date fecha = iter.next().getFecha(); 
			Time hor = iter.next().getHora();
			String dire = iter.next().getDirEntrega();
			Venta vent = ventas.ObtenerVenta(num);
			int monto = vent.getPoseen().PoseenMontoTotal();
			boolean f= iter.next().getFinalizada(); 
			
			// Crear el value object
			VOListGralVentas vv = new VOListGralVentas(num, fecha, hor, dire, monto, f);
			arre.add(vv);
		}
		
		return arre;
	}
	
	public ArrayList<VOListViandasEnVenta> ListarViandasVenta(Venta vent) throws VentaException{
		if (vent.getCantTotal() > 0) { // Si hay viandas cargadas para esta venta
			
			// Llamo a obtenerTodoslosPosee para llenar el VO con los valores que quiero mostrar
			ArrayList<VOListViandasEnVenta> arre = vent.getPoseen().obtenerTodoslosPosee();
			
			// Retorno el arreglo con los VO correspondientes
			return arre;
		}else {
			throw new VentaException(6, "Aun no existen viandas ingresadas para esta venta.\n");
		}
	}
	
	/*
	public Venta[] GuardarVentas (){ //Codigo para guardar las ventas en un archivo		
		Venta arre[] = new Venta[Ventas.size()];
		Iterator<Venta> iter = Ventas.iterator();
		
		while (iter.hasNext()) {
			int i = 0;
			
			// Obtengo el numero de venta, para quedarme con el objeto e insertarlo en el arreglo
			int num = iter.next().getNumero(); 
			Venta av = ventas.ObtenerVenta(num);
			
			//Pensar como hacer o que es lo que pasa con la secuencia poseen dentro de la venta, se hace solo ya que es un objeto en si? esta como serializable
			
			// La inserto en el arreglo
			arre[i] = av;
			
			i++;
		}
		
		return arre;
	}
	
	public void RestaurarVentas (){ 
		//Codigo para respaldar las ventas desde un archivo
	}
	*/
}
