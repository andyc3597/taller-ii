package Sistema.Logica;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import Sistema.Excepciones.*;
import Sistema.Grafica.*;

public interface IFachada extends Remote {
		
	// Requerimiento 1
	public void NuevaVianda (VOVianda vovia) throws ViandaException, RemoteException;
		
	// Requerimiento 2
		public void NuevaVenta (VOVenta voven) throws VentaException, RemoteException;
		
	// Requerimiento 3
	public void AgregarVianda (VOAgrElimVianda vo) throws ViandaException, VentaException, RemoteException;
		
	// Requerimiento 4
	public void EliminarVianda (VOAgrElimVianda vo) throws ViandaException, VentaException, RemoteException;
	
	// Requerimiento 5
	public void FinalizarVenta (VOFinVenta vo) throws VentaException, RemoteException;

	// Requerimiento 6
	public ArrayList<VOListGralVentas> ListadoGralVentas() throws VentaException, RemoteException;
	
	// Requerimiento 7
	public ArrayList<VOListViandasEnVenta> ListadoDetViandasVenta(int numVen) throws RemoteException, VentaException;
	
	// Requerimiento 8
	public void GuardarCambios() throws PersistenciaException, RemoteException;

	// Requerimiento 9
	public void RestaurarCambios() throws RemoteException, PersistenciaException;
	
	// Requerimiento 10
	public ArrayList<VOListGralViandas> ListadoGralViandas() throws ViandaException, RemoteException;
	
	// Requerimiento 11
	public VOVianda ListadoDetVianda(String codVian) throws ViandaException, RemoteException;
	
	// Requerimiento 12
	public ArrayList<VOListGralViandas> ListadoViandasDescrip(String descrip) throws RemoteException, ViandaException;
}