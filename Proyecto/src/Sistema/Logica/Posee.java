package Sistema.Logica;
import java.io.Serializable;


public class Posee implements Serializable{
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	private Vianda vianda;
	private int cantidad;

	// Constructor
	public Posee (Vianda vian, int cant){
		vianda = vian;
		cantidad = cant;
	}
	
	public Vianda getVianda(){
		return vianda;
	}
	
	public int getCantidad(){
		return cantidad;
	}
	
	public void setCantidad(int cant){
		cantidad = cant;
	}
}
