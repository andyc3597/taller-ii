package Sistema.Logica;
import java.io.Serializable;
import java.util.*;

import Sistema.Grafica.VOListViandasEnVenta; 

public class Poseen implements Serializable{
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	ArrayList<Posee> Poseen = new ArrayList<Posee>();
	
	public void InsBack(Posee pos){ 
		Poseen.add(pos);
	}
		
	public boolean PoseenExisteVianda(Vianda via){
		boolean resu = false;
		Iterator<Posee> iter = Poseen.iterator();
		
		while (!resu && iter.hasNext()) {
			Vianda vian = iter.next().getVianda(); 
			if(via == vian) {
				resu = true;
			}
		}
		return resu;
	}
	
	public ArrayList<VOListViandasEnVenta> obtenerTodoslosPosee(){
		ArrayList<VOListViandasEnVenta> arre = new ArrayList<VOListViandasEnVenta>();
		
		Iterator<Posee> iter = Poseen.iterator();
		
		while (iter.hasNext()) {
			Vianda vian = iter.next().getVianda(); // Me quedo con la vianda de Posee
			String descripAd = " - "; // Creo el string con valor por defecto vacio, y si es vegetariana si se lo cargo
			
			// Me quedo con los datos a mostrar de la vianda
			String cod = vian.getCodigo();
			String descrip = vian.getDescripcion();
			int prec = vian.getPrecio();
			
			// Me quedo con la cantidad de unidades
			int cantUni = iter.next().getCantidad();
			
			if (vian instanceof Vegetariana) {
				descripAd = ((Vegetariana) vian).getDescripcionOvo();
			}
			
			VOListViandasEnVenta vv = new VOListViandasEnVenta(cod, descrip, prec, cantUni, descripAd);
			arre.add(vv);

		}
		return arre;
	}
	
	public void PoseenEliminarVianda(Posee pos){
		Poseen.remove(pos);// aca se elimina el objeto posee completo con la cantidad no?
	}
	
	public int PoseenMontoTotal(){
		int cant, monto = 0, precio, precioTotal;
		int tope = Poseen.size(); // Me guardo el tope para no llamar a la funcion size cada vez en el for por cada iteracion
		
		for (int i=0; i<tope; i++) {
			cant = Poseen.get(i).getCantidad();
			precio = Poseen.get(i).getVianda().getPrecio();
			precioTotal = precio * cant;
			monto = monto + precioTotal;
		}
		
		return monto;
	}
	
	public Posee getPosee(Vianda via){
		int tope = Poseen.size();
		boolean encontre = false;
		int i = 0;
		Posee pos = null;
		
		while(i<tope && encontre == false) {
			if (Poseen.get(i).getVianda() == via) {
				encontre = true;
				pos = Poseen.get(i);
			}else {
				i++;
			}
		}
		return pos;
	}
}
