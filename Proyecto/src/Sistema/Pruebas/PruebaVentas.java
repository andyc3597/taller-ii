package Sistema.Pruebas;

import java.sql.Time;
import java.util.Date;
import java.util.Scanner;
import Sistema.Logica.Venta;
import Sistema.Logica.Ventas;

public class PruebaVentas {
	
	public static void main(String[] args) {
		System.out.println("---------------GENERO VENTAS---------------\n");	
		Ventas secventas = null;
			
		System.out.println("---------------CREO UNA NUEVA VENTA---------------\n");
		System.out.println("---------------genero hora---------------\n");
		Time hora = new Time(15, 12, 56);
		System.out.println("---------------genero fecha---------------\n");
		Date fecha = new Date(2020, 05, 12); //AÑO, MES, DIA
		System.out.println("---------------constructor de venta---------------\n");
		Venta venta1 = new Venta (1, fecha, hora, "Direccion de entrega 1234");
		
		System.out.println("---------------HACEMOS UN PRINT A LA VENTA---------------\n");
		
		System.out.println("Datos de la venta: \nNumero: " + venta1.getNumero());
		System.out.println("Fecha: " + venta1.getFecha().getDate() + "/" + venta1.getFecha().getMonth() + "/" + venta1.getFecha().getYear());
		System.out.println("Hora: " + venta1.getHora());
		System.out.println("Direccion: " + venta1.getDirEntrega());
		if(venta1.getFinalizada()) {
			System.out.println("Finalizada: Si");
		}else {
			System.out.println("Finalizada: No");
		}
		System.out.println("Cantidad Total: " + venta1.getCantTotal());
		//RESTA UN PRINT DE POSEEN
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("---------------verifico si la secuencia esta vacía---------------\n");
		if (secventas.Empty()) {
			System.out.println("La secuencia esta vacia\n");
		}else {
			System.out.println("La secuencia NO esta vacia\n");
		}
		System.out.println("---------------INSERTO VENTA EN LA SECUENCIA---------------\n");
		secventas.InsBack(venta1);
		System.out.println("---------------verifico de nuevo si la secuencia esta vacía---------------\n");
		
		if (secventas.Empty()) {
			System.out.println("La secuencia esta vacia\n");
		}else {
			System.out.println("La secuencia NO esta vacia\n");
		}
		
		
	}
}
