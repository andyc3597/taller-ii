package Sistema.Excepciones;

public class VentaException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int codigo;
	private String mensaje;
	
	public VentaException(int cod, String msj){
		codigo = cod;
		mensaje = msj;
	}
	
	public int getCodigo(){
		return codigo;
	}
	
	public String getMensaje(){
		return mensaje;
	}
}
