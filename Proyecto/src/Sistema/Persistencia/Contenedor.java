package Sistema.Persistencia;

import java.io.Serializable;

import Sistema.Logica.Ventas;
import Sistema.Logica.Viandas;

public class Contenedor implements Serializable{
	
	// Codigo automatico
	private static final long serialVersionUID = 1L;
	
	// Atributos
	private Viandas viandas;
	private Ventas ventas;
	
	// Constructor comun
	public Contenedor(Viandas vian, Ventas ven){
		viandas = vian;
		ventas = ven;
	}
	
	// Constructor por defecto (vacio)
	public Contenedor() {
		this.viandas = new Viandas();
		this.ventas = new Ventas();
	}

	// Selectoras
	public Viandas getViandas() {
		return viandas;
	}
	
	public Ventas getVentas() {
		return ventas;
	}
}
