package Sistema.Persistencia;

import java.io.*;

import Sistema.Excepciones.PersistenciaException;
import Sistema.Logica.Vianda;
import Sistema.Logica.Venta;

public class Respaldo{
	public void respaldar (String nomArch, Contenedor arre[]) throws PersistenciaException{
		try{ 
			// Abro el archivo y creo un flujo de comunicaci�n hacia �l
			FileOutputStream f = new FileOutputStream(nomArch);
			ObjectOutputStream o = new ObjectOutputStream(f);
			
			// Escribo el arreglo de viandas en el archivo a trav�s del flujo
			o.writeObject (arre);
			o.close();
			f.close();
			
		}catch (IOException e){ 
			e.printStackTrace();
			String msg = "Error al respaldar.";
			throw new PersistenciaException(1, msg);
		}
	}

	public Contenedor[] recuperar (String nomArch) throws PersistenciaException, ClassNotFoundException{
		try{ 
			// Abro el archivo y creo un flujo de comunicaci�n hacia �l
			FileInputStream f = new FileInputStream(nomArch);
			ObjectInputStream o = new ObjectInputStream(f);
			
			// Leo ambos arreglos de ventas y viandas dentro del contendor
			Contenedor[] arre = (Contenedor[])o.readObject();
			o.close();
			f.close();
			return arre;
		}catch (IOException e){ 
			e.printStackTrace();
			String msg = "Error al recuperar.";
			throw new PersistenciaException(2, msg);
		}
	}

}
