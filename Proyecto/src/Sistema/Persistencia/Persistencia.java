package Sistema.Persistencia;

import java.io.*;
import Sistema.Excepciones.PersistenciaException;

public class Persistencia{
	
	private String nomArch;

	//Constructor
	public Persistencia(String nomArch)
	{
			this.nomArch = nomArch;
	}

	public void respaldar (Contenedor cont) throws PersistenciaException{
		try{
			// Abro el archivo y creo un flujo de comunicaci�n hacia �l
			FileOutputStream f = new FileOutputStream(nomArch);
			ObjectOutputStream o = new ObjectOutputStream(f);
			
			// Escribo el arreglo de viandas en el archivo a trav�s del flujo
			o.writeObject (cont);
			o.close();
			f.close();
			
		}catch (IOException e){ 
			e.printStackTrace();
			String msg = "Error al respaldar.";
			throw new PersistenciaException(1, msg);
		}
	}

	public Contenedor recuperar () throws ClassNotFoundException, IOException{

			Contenedor cont = null;
			// Abro el archivo y creo un flujo de comunicaci�n hacia �l
			try
			{
			FileInputStream f = new FileInputStream(nomArch);
			ObjectInputStream o = new ObjectInputStream(f);
			
			// Leo ambos arreglos de ventas y viandas dentro del contendor
			cont = (Contenedor)o.readObject();
			o.close();
			f.close();
			}
			catch(FileNotFoundException e)
			{
				cont = new Contenedor();
			}
			return cont;

	}

}
